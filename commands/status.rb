require 'rbconfig'
require 'sys-cpu'
require 'vmstat'
require 'facter'

def hostname
  Socket.gethostname
end

def os_info
  os = Facter.value 'os'
  "#{os['name']} #{os['release']['full']}"
end

def cpu_model
  # As of now, CPU model name detection is only supported on Linux.
  # Other platforms will just return the architecture.

  if /linux/i =~ RbConfig::CONFIG['host_os']
    cpu_names = []
    cpus = {}
    Sys::CPU.processors.each do |cpu|
      if cpus.key? cpu.model_name
        cpus[cpu.model_name] += 1
      else
        cpus[cpu.model_name] = 1
      end
    end
    cpus.keys.each do |model|
      cpu_names.push "#{model} x#{cpus[model]}"
    end
  else
    cpu_names.push "Generic #{Sys::CPU.architecture} CPU"
  end

  cpu_names.join ', '
end

def cpu_util
  Vmstat.snapshot.load_average.one_minute
end

def data_size_unit(bytes)
  # Takes input in bytes
  return "#{bytes} B" if bytes < 1024

  # Kibibytes
  size = bytes.to_f / 1024
  return "#{(size * 100).to_i.to_f / 100} KiB" if size < 1024

  # Mebibytes
  size /= 1024
  return "#{(size * 100).to_i.to_f / 100} MiB" if size < 1024

  # Gibibytes
  size /= 1024
  return "#{(size * 100).to_i.to_f / 100} GiB" if size < 1024

  # Tebibytes
  size /= 1024
  return "#{(size * 100).to_i.to_f / 100} TiB" if size < 1024

  # Pebibytes
  size /= 1024
  return "#{(size * 100).to_i.to_f / 100} PiB" if size < 1024
end

def used_mem
  # Reports incorrect value, fix later
  data_size_unit Vmstat.snapshot.memory.active_bytes
end

def total_mem
  data_size_unit Vmstat.snapshot.memory.total_bytes
end

def uptime
  total_seconds = (Time.now - Vmstat.snapshot.boot_time).to_i
  seconds = total_seconds % 60
  minutes = (total_seconds / 60) % 60
  hours = (total_seconds / 3600) % 3600
  days = total_seconds / 86_400
  format('%<days>d days, %<hours>02d:%<minutes>02d:%<seconds>02d',
         days: days, hours: hours, minutes: minutes, seconds: seconds)
end

def cmd_status(message)
  status = "**Server status:\n**"

  # Hostname and OS
  # Don't need to do anything if both are requested to be hidden in config
  if configatron.show_os_info && !configatron.show_hostname
    status += "Running on: #{os_info}\n"
  elsif !configatron.show_os_info && configatron.show_hostname
    status += "Running on: #{hostname}\n"
  elsif configatron.show_os_info && configatron.show_hostname
    status += "Running on: #{hostname} (#{os_info})\n"
  end

  # CPU information
  if configatron.show_cpu_model && !configatron.show_cpu_util
    status += "CPU: #{cpu_model}\n"
  elsif !configatron.show_cpu_model && configatron.show_cpu_util
    status += "CPU: #{cpu_util}% util\n"
  elsif configatron.show_cpu_model && configatron.show_cpu_util
    status += "CPU: #{cpu_model} (#{cpu_util}% util)\n"
  end

  # Memory information
  status += "Memory: #{used_mem} used of #{total_mem}\n" if
    configatron.show_memory

  # System uptime
  status += "Uptime: #{uptime}\n" if configatron.show_uptime

  status += 'CrudBot version 0.1.0 [TESTING]'
  message.reply status
end
