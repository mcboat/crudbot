COMMANDS = {
  'help' => {
    # This description is bound to change!
    desc: 'Get help for CrudBot or a specified command.',
    syntax: [
      'help [<command>]'
    ],
    examples: [
      'help',
      'help status'
    ]
  },
  'status' => {
    desc: 'Get information on the server CrudBot is running on.',
    syntax: [
      'status'
    ],
    examples: [
      'status'
    ]
  },
  'cat' => {
    desc: 'Get a random cat picture, courtesy of the cat owners of CrudButt.',
    syntax: [
      'cat'
    ],
    examples: [
      'cat'
    ]
  }
}.freeze

def cmd_help(message)
  args = message.content.split

  if args.length >= 2
    if COMMANDS[args[1]].nil?
      message.reply "I don't have a command called `#{args[1]}`! :("
      return
    end

    prefix = '&'
    help_str = "**Help for command #{args[1]}**\n"
    help_str += "#{COMMANDS[args[1]][:desc]}\n"
    unless COMMANDS[args[1]][:syntax].nil?
      help_str += "\n__Syntax__\n"
      COMMANDS[args[1]][:syntax].each do |syntax|
        help_str += "\u2022 `#{prefix}#{syntax}`\n".encode 'utf-8'
      end
    end
    unless COMMANDS[args[1]][:examples].nil?
      help_str += "\n__Examples__\n"
      COMMANDS[args[1]][:examples].each do |examples|
        help_str += "\u2022 `#{prefix}#{examples}`\n".encode 'utf-8'
      end
    end
  else
    help_str = "**CrudBot help**\n"
    COMMANDS.keys.each do |cmd|
      help_str += "**`#{prefix}#{cmd}`:** #{COMMANDS[cmd][:desc]}\n"
    end
  end
  message.reply help_str.chomp
end
