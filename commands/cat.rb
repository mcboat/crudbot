require_relative '../classes/floof.rb'

# https://imgur.com/a/8H6HGWi
CATS = {
  'Tuxedo' => Floof.new('Tuxedo', 'Jsn', :BOY),
  'Sock' => Floof.new('Sock', 'Jsn', :GIRL),
  'Cammy' => Floof.new('Cammy', 'Season Leaf', :GIRL),
  'Milo' => Floof.new('Milo', 'Summer', :BOY),
  'Spaz' => Floof.new('Spaz', 'Owen', :BOY)
}.freeze

CAT_PICS = [
  FloofPic.new('EcxUrSO', CATS['Tuxedo']),
  FloofPic.new('Ioz9bkc', CATS['Tuxedo']),
  FloofPic.new('grk40or', CATS['Tuxedo']),
  FloofPic.new('94G8xg3', CATS['Tuxedo']),
  FloofPic.new('e4u0e8T', CATS['Tuxedo']),
  FloofPic.new('Qg3ppOz', CATS['Sock']),
  FloofPic.new('wu5DfVZ', CATS['Sock']),
  FloofPic.new('EwJyNMF', CATS['Sock']),
  FloofPic.new('6jSZxDE', CATS['Cammy']),
  FloofPic.new('DxJdkEF', CATS['Cammy']),
  FloofPic.new('mFNWQuL', CATS['Cammy']),
  FloofPic.new('Dq6cNMA', CATS['Cammy']),
  FloofPic.new('h48FjEs', CATS['Cammy']),
  FloofPic.new('owhm286', CATS['Cammy']),
  FloofPic.new('XDFcNNB', CATS['Milo']),
  FloofPic.new('Id4hVI6', CATS['Spaz'])
].freeze

def cmd_cat(message)
  cat = CAT_PICS.sample
  message.channel.send_file File.open(cat.path),
                            caption: "This funky lil #{cat.gender}'s name " \
                            "is *#{cat.name}*, owned by *#{cat.owner}!* :cat:",
                            tts: false
end
