require 'discordrb'
require 'configatron'
require_relative 'config.rb'

# Commands
require_relative 'commands/status.rb'
require_relative 'commands/help.rb'
require_relative 'commands/cat.rb'

bot = Discordrb::Bot.new token: configatron.token

bot.message do |event|
  if event.message.content.start_with? '&'
    case event.message.content[1..-1].split[0]
    when 'status'
      cmd_status event.message
    when 'help'
      cmd_help event.message
    when 'cat'
      cmd_cat event.message
    end
  end
end

bot.run
