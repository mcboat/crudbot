# A floof (cat, dog, etc).
# Params:
# +name+:: the name of the floof in question ("Tuxedo")
# +owner+:: the name of the floof's owner ("Jsn")
# +gender+:: the gender of the floof (:BOY or :GIRL)
class Floof
  def initialize(name, owner, gender)
    @name = name
    @owner = owner
    @gender = gender
  end

  attr_reader :name
  attr_reader :owner
  attr_reader :gender
end

# A specific picture of a floof (cat, dog, etc)
# Params:
# +url+:: the Imgur URL's ID part. For example, Dq6cNMA
# +floof+:: +Floof+ object that defines what floof the picture is of.
class FloofPic
  def initialize(url, floof)
    @url = url
    @name = floof.name
    @owner = floof.owner
    @gender = floof.gender
  end

  def path
    "resources/cats/#{@url}.jpg"
  end

  attr_reader :name
  attr_reader :owner

  def gender
    @gender == :BOY ? 'dude' : 'gal'
  end
end
