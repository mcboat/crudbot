# CrudBot

A simple, multi-purpose Discord and IRC bot, created as a replacement to the
ill-fated ScykohBot. Supports a variety of commands, from general miscellany
to jokes and memes to useful utilities.

(IRC is coming soon!)

## Install

* Copy `example.config.rb` to `example.config`. Add your bot token and change
  whatever settings you want to.
* Run `bundle install` to fetch the necessary gems.
* Run the bot with `ruby crudbot.rb`.

## Creating a service

On some systems, it may be preferred to create a service to allow the bot to
run automatically. How varies based on how your system is configured, but for
Linux using systemd (arguably the most common server configuration), here's an
example unit file.

```ini
[Unit]
Description=CrudBot Discord bot
After=network.target

[Service]
Type=simple
WorkingDirectory=/path/to/crudbot
ExecStart=/path/to/crudbot/crudbot.rb
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

## Contributing

We use Rubocop to enforce programming style. Read `.rubocop.yml` for more info
on how exactly; it's mostly standard, except for larger max function size.
